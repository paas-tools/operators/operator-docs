# Introduction

This website contains all the information we have gathered on developing, deploying and maintaining operators.

## What is an operator

A Kubernetes operator is a method of packaging, deploying, and managing a Kubernetes application. A Kubernetes application is both deployed on Kubernetes and managed using the Kubernetes API (application programming interface) and `kubectl` tooling.

A Kubernetes operator is an application-specific controller that extends the functionality of the Kubernetes API to create, configure, and manage instances of complex applications on behalf of a Kubernetes user.

It builds upon the basic Kubernetes resource and controller concepts, but includes domain or application-specific knowledge to automate the entire life cycle of the software it manages.

## How do we use operators

Since the conception of the [okd4-install](https://gitlab.cern.ch/paas-tools/okd4-install/) project we have worked on trying to automatize as much as possible the creation of a cluster. To achieve this we have developed multiple operators that interact no only with parts of the clusters but also with services provided by other teams at CERN, like Networking and Authentication. All the developed operators can be found over at https://gitlab.cern.ch/paas-tools/operators.