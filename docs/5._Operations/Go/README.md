# Operations

 - Updating old operators
   - [SDK updates](update-sdk.md)
   - CRD updates
   - Reconcile cycle updates
 - Migrating operators
   - Ensure reconcile cycles do the same thing as before, spec example where certain things were getting overwritten