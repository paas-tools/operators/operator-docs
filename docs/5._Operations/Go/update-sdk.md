# Update SDK version

## overview

It is rarely needed to upgrade operator-sdk on an existing project.

Some motivations for upgrading operator-sdk:

- add new APIs (Custom Resources) to old operator projects - recent versions of operator-sdk don't support creating new APIs in old projects due to changes in kubebuilder layout
- need functionality/bugfix in more recent `controllerutil` library

Example MR to update the SDK on authz-operator: <https://gitlab.cern.ch/paas-tools/operators/authz-operator/-/merge_requests/96>

## process to upgrade to newer operator-sdk

Install latest SDK version: https://sdk.operatorframework.io/docs/installation/ - in my case `v1.33.0`

Install appropriate version of `go`. E.g. SDK 1.33 says it needs go >=1.20 but 1.22 isn't working: I'm getting NullPointerExceptions with go 1.22 when creating APIs. In the end I had to use `go 1.21`. I have this in my `~/.profile`:

```bash
# use go 1.21.7 for operator-sdk 1.33 (NPE error with go 1.22.0)
# Go installed as per https://go.dev/doc/install
export GOROOT=/usr/local/go-1.21.7
PATH="$GOROOT/bin:$PATH"
```

List of migrations for each operator-sdk version: we're supposed to apply all of them until the latest version: https://sdk.operatorframework.io/docs/upgrading-sdk-version/

The operator-sdk is generally backwards compatible and can work with existing projects created by older versions in general. But the go operators' "go plugin" uses kubebuilder as a plugin and this has breaking version changes, e.g. https://sdk.operatorframework.io/docs/upgrading-sdk-version/v1.33.0/

Current "go plugin" version of an operator can be found under plugins in `PROJECT` file, e.g. in authz-operator this was v2-alpha:

```yaml
plugins:
  go.sdk.operatorframework.io/v2-alpha: {}
```

In this situation, to upgrade to operator-sdk v1.33 and latest kubebuilder v4, I had to **recreate the project** as per https://book.kubebuilder.io/migration/migration_guide_gov3_to_gov4

### Recreating project for newer operator-sdk

```bash
ORIG_PROJECT_PATH=~/git/authz-operator
NEW_PROJECT_PATH=/tmp/authz-operator

mkdir -p $NEW_PROJECT_PATH && cd $NEW_PROJECT_PATH
# check old project settings
cat $ORIG_PROJECT_PATH/PROJECT
# create new project using same settings (domain etc.) and new v4 plugin
operator-sdk init --plugins go/v4 --domain cern.ch --owner CERN --repo gitlab.cern.ch/paas-tools/operators/authz-operator
go mod tidy
# recreate the API types. In this operator, OidcReturnURI doesn't have controller code
operator-sdk create api --group webservices --version v1alpha1 --kind ApplicationRegistration --resource=true --controller=true
operator-sdk create api --group webservices --version v1alpha1 --kind BootstrapApplicationRole --resource=true --controller=true
operator-sdk create api --group webservices --version v1alpha1 --kind ProjectLifecyclePolicy --resource=true --controller=true
operator-sdk create api --group webservices --version v1alpha1 --kind OidcReturnURI --resource=true --controller=false

# make a git repo in NEW_PROJECT_PATH to keep track of changes more easily
git init .
git add * .dockerignore .gitignore && git commit -a -m "recreated project for go v4 layout"

# now copy the code from ORIG_PROJECT to the new locations
# API definitions
cp -R ${ORIG_PROJECT_PATH}/api/v1alpha1/* ${NEW_PROJECT_PATH}/api/v1alpha1/
# controller code but not suite_test.go (tests not functional in orig project, if I copy it it breaks go mod tidy)
find ${ORIG_PROJECT_PATH}/controllers/ -name '*.go'  -not -name 'suite_test.go' -exec cp \{} ${NEW_PROJECT_PATH}/internal/controller/ \;
# update package due to new path
find ${NEW_PROJECT_PATH}/internal/ -name '*.go' -exec sed -i -e 's:package controllers:package controller:' \{} \;

# the rest is project-specific... This will vary on different operators

# helpers (code outside of controller code)
cp -R ${ORIG_PROJECT_PATH}/apicache ${ORIG_PROJECT_PATH}/authzapireq ${NEW_PROJECT_PATH}/internal/
rm ${NEW_PROJECT_PATH}/internal/authzapireq/requests_test.go # not functional
# update internal package references due to change of path
find ${NEW_PROJECT_PATH}/internal/ -name '*.go' -exec sed -i -e 's:gitlab.cern.ch/paas-tools/operators/authz-operator/\(apicache\|authzapireq\):gitlab.cern.ch/paas-tools/operators/authz-operator/internal/\1:' \{} \;

# doc, CI, Helm chart etc
cp -R ${ORIG_PROJECT_PATH}/{.gitlab-ci.yml,README.md,Dockerfile,LICENSE,.gitignore,.operator-ci.helm_values_file.template,docs/,chart/} ${NEW_PROJECT_PATH}/
# customized CR samples
cp -R ${ORIG_PROJECT_PATH}/config/samples/{appreg.yaml,webservices_v1alpha1_applicationRegistration2.yaml,oidcreturnuri.yaml,webservices_v1alpha1_projectlifecyclepolicy.yaml,webservices_v1alpha1_bootstrapapplicationrole.yaml} ${NEW_PROJECT_PATH}/config/samples/
# merge notes/ into docs/
cp -R ${ORIG_PROJECT_PATH}/notes ${NEW_PROJECT_PATH}/docs/
```

Edit `Dockerfile`:
- change base image to go 1.20 (cf. req version in go.mod)

Edit `Makefile`:
- update `build` and `run` targets to build `cmd/operator/operator.go` - this is because the original project builds 2 binaries and thus we have 2 go files implementing "main" instead of the single standard `main.go`

Update go modules with additional dependencies from copied code.
This project references the `openshift/api` module but we must not let `go` get the latest version! It needs a version of `openshift/api` compatible with the `go-client` version used by this operator-sdk version (visible in `go.mod`).

After a lot trial and error I got it working with this:

```bash
# For go-client 0.27 (k8s 1.27) the corresponding OKD version is 4.14
go get github.com/openshift/api@release-4.14
# we also use gocron in this project
go get github.com/go-co-op/gocron
```


Run `make manifests` to update the `config/` folder from code comments using `controller-gen`.
A diff revealed some differences with orig project but nothing significant. 
We don't use the `config/` folder much since instead we deploy the operator with our custom `chart/`. The `config/` folder is only used to deploy the generated CRDs from `config/crd/bases`.

### Code adaptations

With the new SDK I had to do some changes to code using the controller-util `Watches` function:

```go
		Watches(&source.Kind{Type: &webservicesv1alpha1.OidcReturnURI{}}, handler.EnqueueRequestsFromMapFunc(
			func(a client.Object) []reconcile.Request {
				log := r.Log.WithValues("Source", "OIDCReturnURI event handler", "Namespace", a.GetNamespace())
				// Fetch the Applications in the same namespace
				applications := &webservicesv1alpha1.ApplicationRegistrationList{}
				if err := mgr.GetClient().List(context.TODO(), applications, &client.ListOptions{Namespace: a.GetNamespace()}); err != nil {
```
=> 
```go
		Watches(&webservicesv1alpha1.OidcReturnURI{}, handler.EnqueueRequestsFromMapFunc(
			func(context context.Context, a client.Object) []reconcile.Request {
				log := r.Log.WithValues("Source", "OIDCReturnURI event handler", "Namespace", a.GetNamespace())
				// Fetch the Applications in the same namespace
				applications := &webservicesv1alpha1.ApplicationRegistrationList{}
				if err := mgr.GetClient().List(context, applications, &client.ListOptions{Namespace: a.GetNamespace()}); err != nil {
```

```go
Watches(
			// this source enables the foreground reconciler and the lifecycle controller to send reconcile requests for
			// ApplicationRegistration resources that should be synced with Authz API in the background.
			&source.Channel{Source: r.AppStatusRefreshRequests},
			&handler.EnqueueRequestForObject{},
		).
```
=>
```go
WatchesRawSource(
			// this source enables the foreground reconciler and the lifecycle controller to send reconcile requests for
			// ApplicationRegistration resources that should be synced with Authz API in the background.
			&source.Channel{Source: r.AppStatusRefreshRequests},
			&handler.EnqueueRequestForObject{},
		).
```

Removed this line, which was only necessary because of older SDK:

```go
		For(&webservicesv1alpha1.ApplicationRegistration{}). // TODO: this watches ApplicationRegistrations, we don't want to but this is a requirement until we update operator-sdk, then we can remove this.
```

Removed tests from CI (not functional)

Finally we need to merge customizations to the `main.go`.
In the authz-operator it's a bit special because we had replaced  `cmd/main.go` with `cmd/operator/operator.go` and `cmd/softdeletion/lifecycle-delete.go`, but `cmd/operator/operator.go`  is close to the original `main.go`.

I started with the original project's `cmd/operator/operator.go` and backported the new things generated by operator-sdk in the new project's `cmd/main.go`:

```bash
cp -R ${ORIG_PROJECT_PATH}/cmd/ ${NEW_PROJECT_PATH}/
# update internal package references due to change of path
find ${NEW_PROJECT_PATH}/cmd/ -name '*.go' -exec sed -i -e 's:gitlab.cern.ch/paas-tools/operators/authz-operator/\(apicache\|authzapireq\):gitlab.cern.ch/paas-tools/operators/authz-operator/internal/\1:' \{} \;
find ${NEW_PROJECT_PATH}/cmd/ -name '*.go' -exec sed -i -e 's:gitlab.cern.ch/paas-tools/operators/authz-operator/controllers:gitlab.cern.ch/paas-tools/operators/authz-operator/internal/controller:' \{} \;
```

Then I had to fix `controllers.XXX` -> `controller.XXX` references in code. I also addressed deprecation and other warnings.

Then I updated this block to match the new `main.go`:

```go
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&healthAddr, "health-addr", ":8081", "The address the healthz endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
```
=>
```go
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
```

```go
	enableLeaderElection := false
	metricsAddr := ""
	probeAddr := ""
	namespace := ""
	// Settings for the Application Portal management link generated by the PojectLifecyclePolicy controller.
	// These are set globally because it is likely that if they change, we should update all existing links.
	applicationPortalBaseUrl := ""
	applicationPortalLinkText := ""
	applicationCategoryUndefinedLinkText := ""
	applicationCategoryTestLinkText := ""
	applicationCategoryPersonalLinkText := ""
	applicationCategoryOfficialLinkText := ""
```
=>
```go
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	var namespace string
	// Settings for the Application Portal management link generated by the PojectLifecyclePolicy controller.
	// These are set globally because it is likely that if they change, we should update all existing links.
	var applicationPortalBaseUrl string
	var applicationPortalLinkText string
	var applicationCategoryUndefinedLinkText string
	var applicationCategoryTestLinkText string
	var applicationCategoryPersonalLinkText string
	var applicationCategoryOfficialLinkText string
```

```go
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		HealthProbeBindAddress: healthAddr,
		Port:                   9443,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "d0077095.webservices.cern.ch",
		Namespace:              namespace,
	})
```
=>
```go
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "d0077095.cern.ch",
		// LeaderElectionReleaseOnCancel defines if the leader should step down voluntarily
		// when the Manager ends. This requires the binary to immediately end when the
		// Manager is stopped, otherwise, this setting is unsafe. Setting this significantly
		// speeds up voluntary leader transitions as the new leader don't have to wait
		// LeaseDuration time first.
		//
		// In the default scaffold provided, the program ends immediately after
		// the manager stops, so would be fine to enable this option. However,
		// if you are doing or is intended to do any operation such as perform cleanups
		// after the manager stops then its usage might be unsafe.
		// LeaderElectionReleaseOnCancel: true,
	})
```

And add before `setupLog.Info("starting manager")`:

```go
	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}
```

The above changes need change in the Deployment in the Helm chart too: `--enable-leader-election` => `--leader-elect`


And finally:

```bash
go mod tidy
go mod vendor # vendor dependencies, makes sure we can always rebuild
```

### Import changes into main project

Once happy with the result, I did this to merge my new project into the old git repo:

```bash
cd $ORIG_PROJECT_PATH
# create new branch in original project
git checkout -b update-operator-sdk
# import changes from new project
git remote add newproject $NEW_PROJECT_PATH/.git
git fetch newproject
# wipe folder contents to match new project's starting point
# (keeping important git-ignored folders: .git and .vscode in my case)
rm -r $(find . -mindepth 1 -maxdepth 1 -not -name .git -not -name .vscode)
git commit -a -m "Remove everything to recreate project with newer sdk"
# apply new project's commit history
git cherry-pick ..newproject/master
# now push & open MR!
```

