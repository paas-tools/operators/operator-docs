# Copy files between PVCs

One of the common operations to perform within a migration is to move files (assets) between the old instance and the new one.

## Semi-automated process to copy files from OKD3 to OKD4

For semi-automated migrations to app-catalogue, one can use this script from paas-migration: <https://gitlab.cern.ch/paas-tools/paas-migration/-/tree/master/migration/rsync-pvc-contents.sh>

See example usage in <https://gitlab.cern.ch/paas-tools/paas-migration/-/tree/master/migration/migrate-nexus.sh>

## Manual process to copy files from OpenShift 3 to OKD4

In brief, this can be easily done by provisioning a temporary pod, mount both (or several, depending the needs) volumes, and copy files over.

In the following example, let's copy files from an instance in OKD3 to OKD4. Note that this procedure can be also applied to OKD4 -> OKD4 if needed.

!!! info
    To perform such operations, the performer user must belong to the `openshift-admins` e-group, having administrator permissions to the targeted clusters. As a consequence of belonging to the `openshift-admins` e-group, the performer user will have also access to the Openstack projects, also needed to fetch the appropriate access keys, etc.

```bash
# Login as admtrobo into OKD3
oc login --server=https://openshift.cern.ch -u <username>

sourceProject=<xxx>
oc project $sourceProject
oc run -it --rm -n $sourceProject rsync-demo --privileged --restart=Never --image=gitlab-registry.cern.ch/paas-tools/openshift-client --command -- bash
# inside the container

##########################
# Mount OKD3 volume
##########################

# Username of the performer user (e.g., admtrobo)
username=<username>

sourceProject=<xxx>
osProjectName="IT OpenShift PAAS"

# obtain from running on lxplus8: 
# openstack catalog show manilav2 -f json | jq -r '.endpoints[] | select(.interface == "public") | .url'
MANILA_URL=<xxx>
# obtain from running on lxplus8: (token valid for 24h)
# openstack token issue -f json | jq -r '.id'
OPENSTACK_MANILA_SECRET=<xxx>

# Login as adm in OKD3 inside the pod.
oc login --server=https://openshift.cern.ch -u $username

# Get name of the pvc
# Note we use test("my-data") to retrieve a pvc that matches the name my-data.
okd3PVC=$(oc get pvc -n $sourceProject -o json | jq -r '.items[] | select(.metadata.name | test("my-data")) .spec.volumeName')
echo $okd3PVC

# Grab the export location url, kinda `137.138.121.135:6789,188.184.85.133:6789,188.184.91.157:6789:/volumes/_nogroup/xxx`
cephMon=$(oc get pv/$okd3PVC -o json | jq -r '.spec.csi.volumeAttributes | (.monitors +  ":" + .rootPath)')
echo $cephMon

# Get the Access Key of the share
okd3SecretRef=$(oc get pv/$okd3PVC -o json | jq -r '.spec.csi.nodeStageSecretRef | ("secret/" + .name +  " -n " + .namespace)')
okd3PVCAccessKey=$(oc get $okd3SecretRef -o json | jq -r .data.userKey | base64 -d)
echo $okd3PVCAccessKey

# mount the source OKD3 volume with read-only access.
mkdir -p /mnt/src && mount -t ceph -o ro,name=$okd3PVC,secret=$okd3PVCAccessKey $cephMon /mnt/src
ls -al /mnt/src

##########################
# Mount OKD4 volume
##########################

destProject=<xxx>
#osProjectName='OKD staging'
osProjectName='OKD App Catalogue'

# Login as adm in OKD4 inside the pod
oc login --server=https://api.app-catalogue.okd.cern.ch -u $username

okd4PVC=$(oc get pvc -n $destProject -o json | jq -r '.items[] | select(.metadata.name | test("my-app")) .spec.volumeName')
echo $okd4PVC

# Annotate the path
ITEM=$(oc get pv/$okd4PVC -o json)
NAMESPACE_CSI_DRIVER=$(echo "$ITEM" | jq -r '.spec.csi.nodeStageSecretRef.namespace')
MANILA_SHARE_ID=$(echo "$ITEM" | jq -r '.spec.csi.volumeAttributes.shareID')
MANILA_SHARE_ACCESS_ID=$(echo "$ITEM" | jq -r '.spec.csi.volumeAttributes.shareAccessID')
MANILA_EXPORT_LOCATIONS=$(curl -X GET -H "X-Auth-Token: $OPENSTACK_MANILA_SECRET" -H "X-Openstack-Manila-Api-Version: 2.51" $MANILA_URL/shares/$MANILA_SHARE_ID/export_locations)

# Stores monitors and path of the PV, similar to
# 137.138.121.135:6789,188.184.85.133:6789,188.184.91.157:6789:/volumes/_nogroup/337f5361-bee2-415b-af8e-53eaec1add43
cephMon=$(echo $MANILA_EXPORT_LOCATIONS | jq -r '.export_locations[]?.path')

# Get the Access key of the share
MANILA_ACCESS_RULES=$(curl -X GET -H "X-Auth-Token: $OPENSTACK_MANILA_SECRET" -H "X-Openstack-Manila-Api-Version: 2.51" $MANILA_URL/share-access-rules/$MANILA_SHARE_ACCESS_ID)
okd4PVCAccessKey=$(echo $MANILA_ACCESS_RULES | jq -r '.access.access_key')
echo $okd4PVCAccessKey

# mount the OKD4 volume, this time without read-only access.
mkdir -p /mnt/dest && mount -t ceph -o name=$okd4PVC,secret=$okd4PVCAccessKey $cephMon /mnt/dest
ls -al /mnt/dest/

######################
## Operations
######################

# Copy specific file over
rsync -a --del --progress /mnt/src/my-file.db /mnt/dest/my-file.db

# Copy all files over
rsync -a --del --progress /mnt/src/* /mnt/dest/

##
# IMPORTANT: DO NOT FORGET SET UP permissions on target pvc
##

# Set proper ownership
ls -al /mnt/src/
ls -al /mnt/dest/
chown 1000xxxxx:root -R /mnt/dest/
```
