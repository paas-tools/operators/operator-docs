# Deploy a new version

To deploy a new version of the operator, we will use [OLM (Operator Lifecycle Manager)](https://olm.operatorframework.io/).

FFI: <https://docs.okd.io/latest/operators/understanding/olm/olm-understanding-olm.html>

According to <https://gitlab.cern.ch/paas-tools/infrastructure-ci/-/blob/master/operator-ci-templates/DockerImages.gitlab-ci.yml>, the CI is configured to deploy changes to the staging environment from any branch. This also includes the main branch. For deploying changes into a production cluster, a new tag is needed to generate the appropriate job.

In short:

* **Staging cluster**: any branch (including `master`).
* **Production cluster**: create a new tag using the version number (`VERSION` in `.gitlab-ci.yml`), e.g. `0.0.1`

For both cases, go to your GitLab repository, mouse over `CI/CD` > Pipelines > point the appropriate job and execute the `OLM Deployment (staging)` or `OLM Deployment (production)` jobs accordingly.

Note that as stated in the registry poll interval value configured for each respective catalog source (see `operators-catalogue.<operator-name>.catalogSource.spec.updateStrategy.registryPoll.interval` element under <https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/chart/values-app-catalogue.yaml> or <https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/chart/values-app-cat-stg.yaml>), the deployment might take some minutes to take effect.
