# First Operator

As an introductory step we recommend going over the [tutorial](https://sdk.operatorframework.io/docs/building-operators/golang/tutorial/) on the Operator-SDK project in order to get familiar with the OperatorSDK CLI and what it does.

For OKD4 operators we typically use:

-  `--domain=webservices.cern.ch` 
- a separate `--group=<myoperator>` for each operator
- a GitLab project under <https://gitlab.cern.ch/paas-tools/operators>
