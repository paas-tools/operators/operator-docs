# Set up a development environment

Check the [Introduction](../1-introduction.md) for the basic requirements.

## Docker environment

Clone the git repository of your operator:

```bash
git clone https://gitlab.cern.ch/paas-tools/operators/authz-operator.git
cd authz-operator
```

Boot up a container:

```bash
# Export your kubeconfig path to mount it later on the docker container, so we have access to the
# cluster inside the container
export KUBECONFIG=<FULL PATH TO CLUSTER KUBECONFIG>
# Run an Operator-SDK docker image and mount both the kubeconfig and the project directory
docker run --rm -it -v ${KUBECONFIG}:/root/.kube/config -v <FULL PATH TO PROJECT FOLDER>:/project -w /project gitlab-registry.cern.ch/paas-tools/operators/operator-sdk-client:latest
```

### Install packages specific to the operator

The Operator-SDK image is [maintainted by us](https://gitlab.cern.ch/paas-tools/operators/operator-sdk-client/), we try to keep the image as small as possible, so it doesn't have a lot of packages installed (you can see which packages are installed in the [Dockerfile](https://gitlab.cern.ch/paas-tools/operators/operator-sdk-client/-/blob/master/Dockerfile)). Hence, sometimes if we want to run some code that has dependencies we also have to install them before testing out operator:

```bash
pip-3.6 install --no-cache-dir -r requirements.txt
```

## Local environment

- Install Golang:
- Install Openshift CLI: <https://paas.docs.cern.ch/1._Getting_Started/2-installing-cli/>
- Install OpetatorSDK:
- Install packages specific to the operator.
