# First Operator

As an introductory step we recommend going over the [tutorial](https://sdk.operatorframework.io/docs/building-operators/helm/tutorial/) on the Operator-SDK project in order to get familiar with the OperatorSDK CLI and what it does.
