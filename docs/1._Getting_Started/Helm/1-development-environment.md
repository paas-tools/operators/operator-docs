# Set up a development environment

Check the [Introduction](../1-introduction.md) document for setting up the basic requirements.

## Docker environment and requirements

Clone the git repository of your operator:

```bash
git clone https://gitlab.cern.ch/wordpress/wordpress-operator.git
cd wordpress-operator
```

Boot up a container:

```bash
# Run a Docker cli image and mount the project directory
# We assume the source code is under D:\wordpress\wordpress-operator
docker run -it --rm -v "D:\wordpress\wordpress-operator:/wordpress-operator" -v /var/run/docker.sock:/var/run/docker.sock -w /wordpress-operator docker:19.03.1
```

Install the needed tools and the corresponding operator-sdk version:

!!! info
    Refer to <https://sdk.operatorframework.io/docs/installation/> to pick up the appropriate version of the Operator SDK toolkit.

```bash
apk add make curl gnupg
# Add glibc, requisite for opm
wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.33-r0/glibc-2.33-r0.apk
apk add glibc-2.33-r0.apk

export OPERATOR_SDK_VERSION=1.10.0
export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
export OS=$(uname | awk '{print tolower($0)}')
export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v${OPERATOR_SDK_VERSION}
curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}
gpg --keyserver keyserver.ubuntu.com --recv-keys 052996E2A20B5C7E
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt.asc
gpg -u "Operator SDK (release) <cncf-operator-sdk@cncf.io>" --verify checksums.txt.asc
grep operator-sdk_${OS}_${ARCH} checksums.txt | sha256sum -c -
chmod +x operator-sdk_${OS}_${ARCH} && mv operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk
```

Refer now to the [Helm Development](../2._Development/Helm/README.md) section for development purposes.
