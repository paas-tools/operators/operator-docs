# Introduction

In order to develop your first operator and start playing with it, it is handy to first create a development environment with all the tools that you will need, for this you will need:

1. A source-code editor
2. OpenShift CLI and OperatorSDK CLI installed
3. A development cluster
4. Necessary tools to interact

Refer to the different subfolders for specific requirements.

## Source-code editor

We recommend to use [Visual Studio Code](https://code.visualstudio.com/) a freeware source-code editor made by Microsoft for Windows, Linux and macOS. It's great because it had already built in debugging tools that have been proven essential when trying to find annoying bugs, and given the extensive community support you will most likely be able to find extensions to customize the editor to your precise needs.

## Development cluster

An Openshift 4 development cluster can be created by following the instructions [here](https://okd-internal.docs.cern.ch/development/#provision-a-development-okd4-cluster).

## Necessary tools to interact

We will always deploy our Operators on an Openshift cluster so in order to interact with the cluster you will need the OpenShift CLI instructions on how to install it can be found [here](https://paas.docs.cern.ch/1._Getting_Started/2-installing-cli/).

In order to bootstrap our development process of Operators we have standardized the use of the [OperatorSDK](https://sdk.operatorframework.io/) which is an open source toolkit to manage Operators, in an effective, automated, and scalable way.
