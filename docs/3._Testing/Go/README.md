# Testing

This section of the operator's documentation is supposed to hold all the information related with operator testing, from local testing all the way to fully automated integration tests running on GitLab.

There might be some differences depending on if you're developing an Ansible operator or a Golang operator but for the majority of cases, the testing workflow should be the same, we will also try to mention when certain differences exist.

## Manual Testing

Before delving into the different automatic testing methodology we will cover how to test your operators manually which extremely useful in early developing stages and when we introduce and want to test new features.

#### Export necessary variables

Sometimes some operators also take as input some ENV variables that contain secrets; then for us to run our operator we have to set this ENV variables properly.

```bash
# Create a file where you define your secret ENV
# variables and then, source that file
source secrets.sh
```

### Run operator with Visual Studio Code Debugger

For Golang operators we are also able to test/debug them using the Visual Studio Code debugger. To do this you `json` config file should look like this:

```json
{
 "version": "0.2.0",
 "configurations": [
   {
     "name": "Launch",
     "type": "go",
     "request": "launch",
     "mode": "auto",
     "program": "${fileDirname}",
     "env": {"KUBECONFIG":"<FULL PATH TO CLUSTER KUBECONFIG>", "WATCH_NAMESPACE":"default"},
     "args": [
       "--my-parameter-flag", "alice",
       "--my-parameter-flag", "bob"
     ]
   }
 ]
}
```

### Run the operator with operator-sdk

Finally we can run our operator, we can do this by inputing:

```bash
operator-sdk run --local --watch-namespace ${WATCH_NAMESPACE}
```

The `operator-sdk run` supports a vast amount of flags which allows you to deploy your operator in many ways, so if something is not working try `operator-sdk run -h` and you might find a flag to help solve your problems.

### Unit Tests

Unit tests are always great to have not only because they test individual behaviours but also because we are able to run them many more times in a short period of time than we are able to run integration tests. So we strongly encourage the creation of these types of tests.

#### Ansible

Ansible's operators can be unit tested using [Molecule](https://github.com/ansible-community/molecule) a testing framework that aid in the development and testing of Ansible roles. The official [Operator-SDK docs](https://sdk.operatorframework.io) have a dedicated [section](https://sdk.operatorframework.io/docs/building-operators/ansible/testing-guide/#test-local) only to these types of tests with some guidelines.

Remember that sometimes Ansible operator might be executing some external program such as a Python script for instance, these can be also unit tested.

#### Golang

Golang's operators can be unit tested by using the default Golang [package testing](https://golang.org/pkg/testing/) or any other Golang test framework here is a brief [list with examples](https://github.com/bmuschko/go-testing-frameworks).

We can also use the `envtest` framework as it was done in the [drupalsite-operator](https://gitlab.cern.ch/drupal/paas/drupalsite-operator/-/blob/master/controllers/drupalsite_controller_test.go). This framework coupled with the ability to [mock API resources](https://gitlab.cern.ch/drupal/paas/drupalsite-operator/-/tree/master/testResources/mock_crd) that aren't available in the bare-bones APIserver that `envtest` sets up, provides a great way to test our code.


### E2E tests

TODO not much experience but we would want to:
- How to write and examples
- How to set up CI to run this (this would need operator-CI to work)

### Integration Tests

Nowadays, our cluster provisioning jobs take a very long time to run, because of this we strongly suggest that when developing operators you should first test them locally and only then test them against an on the fly provisioned cluster, otherwise we will just waste a lot of resources fixing small bugs. Instructions on how to run the cluster integration tests on your cluster can be found [here](https://okd-internal.docs.cern.ch/development/#local-development-work-on-cluster-configuration)
