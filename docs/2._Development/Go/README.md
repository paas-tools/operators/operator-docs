# Development

Developing an operator is no easy task and as other pieces of software when developing it we are prone to making mistakes. After some experience developing operators we have come up with a workflow that helps not only reducing the amount of bugs that reach production but also helps with the maintainability of our operators:

1. [Define a CRD](./1-defining-a-crd.md);
1. [Configure `main.go`](./2-main-config.md);
1. [Code the reconcile loop](./3-reconcile-loop.md);
1. [Write unit golang tests](../../3._Testing/Go/README.md#unit-test);
1. [Run operator in debug mode with VSCode or OperatorSDK](../../3._Testing/Go/README.md#run-operator-with-visual-studio-code-debugger);
1. [Write e2e tests](../../3._Testing/Go/README.md#);
1. Make sure the operator is working as intended;
1. [Determine deploy strategy and necessary steps](../../4._Deployment/Go/README.md);
1. Write integration tests on okd4-install;
