# Logging

Operators set the logger for all operator logging in `main.go`. On the example below we can see how to set up the operator logging:

```golang
package main 

import (
    ...
	"time"

    "go.uber.org/zap/zapcore"
    "sigs.k8s.io/controller-runtime/pkg/log/zap"
    zaplogfmt "github.com/sykesm/zap-logfmt"
    ...
)
...
	// Add the zap logger flag set to the CLI. The flag set should
	// be added before calling flag.Parse().
	opts := zap.Options{}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	// Format logs timestamp to time.RFC3339
	// Reference: https://sdk.operatorframework.io/docs/building-operators/golang/references/logging/#custom-zap-logger
	configLog := uzap.NewProductionEncoderConfig()
	configLog.EncodeTime = func(ts time.Time, encoder zapcore.PrimitiveArrayEncoder) {
		encoder.AppendString(ts.UTC().Format(time.RFC3339))
	}
	logfmtEncoder := zaplogfmt.NewEncoder(configLog)
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts), zap.Encoder(logfmtEncoder)))
...
```

The format of the logs will be similar too:
```bash
ts=2022-12-14T10:37:19Z level=info logger=controller.userprovideddirectory msg="Starting workers" reconcilergroup=webeos.webservices.cern.ch reconcilerkind=UserProvidedDirectory workercount=1
ts=2022-12-14T10:37:19Z level=info logger=controllers.UserProvidedDirectory msg="Reconciling request"
ts=2022-12-14T10:37:19Z level=info logger=controllers.WebeosSite msg="Route updated name"
```

In addition, we can set custom log levels to increase verbosity by passing the `--zap-log-level=<LOG_LEVEL>` flag in the operator. Ref: <https://sdk.operatorframework.io/docs/building-operators/golang/references/logging/>.
