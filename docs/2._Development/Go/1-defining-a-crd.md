# Defining a CRD

Defining the CRD of an operator it is the first and one of the most important steps of the development. It's important that this is done properly as usually mistakes in this step, if in the meantime implementation of the operator has started, might then require not only a long time to fix but also a lot of resources. Hence, we strongly recommend that first before typing a single line of code to create a design document where we clarify:

- Purpose of the CRD, what is the objective of the CRD;
- An example of an instance of the CR in YAML with comments on the fields;

This document should then be reviewed by all the team members involved in the scope o the project.

When you have reached a solid design, of your CRD the next step is to code its types in Go. This process is defined into two parts:

- Defining the `Spec`: part of the CR that users will be able to modify;
- Defining the `Status`: part of the CR that will be used by the Operator to report back status;

## Example

For OKD4 operators we typically use:

-  `--domain=webservices.cern.ch` 
- a separate `--group=<myoperator>` for each operator
- a GitLab project under <https://gitlab.cern.ch/paas-tools/operators>

```bash
operator-sdk init --domain webservices.cern.ch --repo gitlab.cern.ch/paas-tools/operators/sitedetails-operator
# create a cluster-scoped custom resource with Group=sitedetails.webservices.cern.ch, Version=v1alpha1, Kind=HostnameInfo
operator-sdk create api --group sitedetails --version v1alpha1 --kind HostnameInfo --resource --controller --plural=hostnameinfos --namespaced=false
```

## CRD Group, Version and Kind

### Group

It's important that CRDs being managed by operators that are deployed independently do NOT share the same API group. This is important as it allows us and other system components to easily determine what APIs are available on a given cluster.

General recommendations:

- use `<operatorname>.webservices.cern.ch` as the API group for operators managing web sites (so CRDs full names look like `<mycrd>.<operatorname>.webservices.cern.ch`); examples: webeos (`userprovideddirectories.webeos.webservices.cern.ch`), drupal (`drupalsites.drupal.webservices.cern.ch`)
- for generic infrastructure operators that could be useful outside webservices use `<operatorname>.operator.cern.ch`. example: example: landb operator (`delegateddomains.landb.operator.cern.ch`)

NB: `webservices.cern.ch` itself is already used by the authz operator.

### Markers for Config/Code

The OperatorSDK uses Kubebuilder to generate the YAML manifests that define the CRD that you specify in Go code. Apart from generating the different types that will be present in the `Spec` and `Status` it is also able to generate some extra utility code. This code and config generation is controlled by the presence of special “marker comments” in Go code.

Markers are single-line comments that start with a plus, followed by a marker name, optionally followed by some marker specific configuration:

```golang
 // +kubebuilder:validation:Optional
 // +kubebuilder:validation:MaxItems=2
```

These can be used to control multiple things, from CRD Generation, CRD Validation to RBAC. An extensive list can be seen in the [Kubebuilder documentation](https://book.kubebuilder.io/reference/markers.html).
2
We will not go over all the markers that we could use, instead we will focus on some that we consider essential. One of those are validation markers. Kubernetes/Openshift is able to enforce some basic validation on resource creation, this is also valid for CR's. However, in order for this validation to take place, you have to specify what validation should be enforced. Like the following marker comment:

```golang
 // +kubebuilder:validation:MinLength=1
```

This marker for instance specifies that the field after it has to have at least the size of one character. Here is a snippet of code where we can see annotations being used.

```golang
...
 // Description defines the purpose of the App
 // +kubebuilder:validation:Required
 // +kubebuilder:validation:MinLength=1
 Description string `json:"description"`
...
```

This piece of code enforces that when the CR is created the field `Description` will be required and that the minimum size of it is one character. Type validation is important as it will allow us to in most cases not having to worry about user input. Obviously in some more complex cases this validation might not be enough and sanitization of input will be required. The OperatorSDK when generating the template of a new API will by itself include all the necessary marker comments, and in most cases only validation markers for the multiple fields of the CRD will need to be added. A full list of validation markers can be found [here](https://book.kubebuilder.io/reference/markers/crd-validation.html).

### Defining the Spec

When defining the `Spec` of a CRD have into consideration the following aspects:

**Reusing types:** If your CRD is going to have types in common with other CRD's you should, instead of re-defining those types, import them from the project where they were defined.

Examples:

- [ApplicationRegistration Spec Type](https://gitlab.cern.ch/paas-tools/operators/authz-operator/-/blob/master/api/v1alpha1/applicationregistration_types.go#L24)

### Defining the Status

As of March 2021 the Kubernetes recommendation for `Status` is to use the Conditions pattern. This pattern is also supported by the controller-runtime library that provides useful functions to help in the management of these Conditions. Objects may report multiple conditions, and new types of conditions may be added in the future or by 3rd party controllers. Therefore, conditions are represented using a list/slice of objects, where each condition has a similar structure. This collection should be treated as a map with a key of type.

Mode detailed information can and should be read in the [k8s API Conventions](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-architecture/api-conventions.md#typical-status-properties)

### useful `controller-gen` options

Whenever `*_types.go` file is updated, `make generate & make manifests` commands have to be run in order to regenerate the final `CRD` definition. By default `controller-gen` outputs the file in `config/crd/bases` directory, but in some cases we would like to use a different directory for that. This can be achieved by modifying `manifests` target in the `Makefile` generated by `operator-sdk`. The following argument added to the invocation of `controller-gen` will instruct the tool to place all artifacts under `chart/crds` (that might be useful e.g. when we maintain a Helm chart for the operator).

```text
output:crd:artifacts:config=chart/crds
```
