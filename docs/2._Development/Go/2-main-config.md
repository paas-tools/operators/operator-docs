# Main configuration

The `main.go` is the entrypoint for our operator, and it's where the initialization/configuration of the controller-runtime will happen.

## Instantiating Resources

This section covers how to instantiate Kubernetes resources whether they are native or custom from within a Golang operator that runs a [Manager](https://godoc.org/github.com/kubernetes-sigs/controller-runtime/pkg/manager#Manager). There are three ways of doing such a thing and the difference between each one comes down to how the project is set up and how the resources you are trying to instantiate are packaged.

The three ways we have experience with are:

- **Instantiating native resources**: resources that are native to plain Kubernetes (e.g namespaces, configmaps, roles, rolebindings, etc);
- **Instantiating a custom resource that provides an API package**: these are resources that are usually managed by other operators and usually have a well-defined GVK;
- **Instantiating resources that do not provide an API package**: these are resources that similarly to the previous ones are custom resources managed by a certain operator but are not bound by group and version (e.g. DNSEndpoint);

This section won't cover owner referencing, but it's a crucial point of study.

### Instantiating native resources

To instantiate a native resource you simply import the package where that resource is defined, and then you should have access to it. For instance consider the following example of instantiating a role binding:

```golang
import  v1 "k8s.io/api/rbac/v1"
...
 roleBinding := &v1.RoleBinding{}
```

### Instantiating resources that provide an API package

This case is very similar to the previous one, but we have to modify the file `main.go` of our operator. Here we will have to pass to the manager the scheme of the resource we want to instantiate. This is done by calling the `AddToScheme()` function for your 3rd party resource and pass it to the Manager's scheme via `mgr.GetScheme()` as you can see the example:

```golang
import routev1 "github.com/openshift/api/route/v1"

func main() {
 ...
 // Adding the routev1
 if err := routev1.AddToScheme(mgr.GetScheme()); err != nil {
   log.Error(err, "")
   os.Exit(1)
 }
 ...
}
```

After this you will be able to instantiate resources like if they were native resources:

```golang
import routev1 "github.com/openshift/api/route/v1"
...
 route := &routev1.Route{}
```

### Instantiating resources that do not provide an API package

This case is similar to the previous one but here we can not simply call the function `AddToScheme()` since the resource we want to create does not provide an API package.
Hence, we will need to use the `SchemeBuilder` package from controller-runtime to initialize a new scheme builder that can be used to register the 3rd party resource with the manager's scheme, as you can see in the example where we will register the resource `DNSEndpoint`:

```golang
import (
 ...
   "k8s.io/apimachinery/pkg/runtime/schema"
   "sigs.k8s.io/controller-runtime/pkg/scheme"
 ...
  // DNSEndoints
  externaldns "github.com/kubernetes-incubator/external-dns/endpoint"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func main() {
 ....

 log.Info("Registering Components.")

 schemeBuilder := &scheme.Builder{GroupVersion: schema.GroupVersion{Group: "externaldns.k8s.io", Version: "v1alpha1"}}
 schemeBuilder.Register(&externaldns.DNSEndpoint{}, &externaldns.DNSEndpointList{})
 if err := schemeBuilder.AddToScheme(mgr.GetScheme()); err != nil {
   log.Error(err, "")
   os.Exit(1)
 }
 ....
}
```

After this you will be able to instantiate resources like if they were native resources:

```golang
import externaldns "github.com/kubernetes-incubator/external-dns/endpoint"
...
 dnsEndpoint := &externaldns.DNSEndpoint{}
```

## Multi thread reconcyle loops

TODO, we still do not have experience with this

https://github.com/kubernetes-sigs/controller-runtime/blob/master/pkg/internal/controller/controller.go#L46

## Multiple reconcile loops

In some cases we might want our operator to manage two CRDs at the same time in different reconcile loops. For this we simply have to
add to our `main.go` the following lines with the propper configuration. In this example we were adding a reconcile loop for the CRD `UserProvidedDirectory`.

```golang
 if err = (&controllers.UserProvidedDirectoryReconciler{
   Client: mgr.GetClient(),
   Log:    ctrl.Log.WithName("controllers").WithName("UserProvidedDirectory"),
   Scheme: mgr.GetScheme(),
 }).SetupWithManager(mgr); err != nil {
   setupLog.Error(err, "unable to create controller", "controller", "UserProvidedDirectory")
   os.Exit(1)
 }
```

Afterwards we can start coding the reconcile loop that will handle our extra CRD.