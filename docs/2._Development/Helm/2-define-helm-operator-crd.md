# Define the Helm Operator CRD

Once we have created our scaffolding project, it is time to define the proper CRD. Unlike Go, in the Helm operator this CRD must be configured manually.

Once the Helm operator is created, the operator sdk creates a skeleton of this CRD under `config/crd/bases/<group>_<operator-name>s.yaml`

An example of a CRD can be found under <https://gitlab.cern.ch/wordpress/wordpress-operator/-/blob/master/config/crd/bases/wordpress.webservices.cern.ch_wordpresses.yaml>

For each new CRD:

1. copy the `status` field definition from an existing CRD (see example above): it's the same for all CRDs managed by a Helm operator
2. define a schema for the `spec` based on what we want users to be able to set in the Helm chart's `values.yaml`
3. ideally, provide a meaningful example in `config/samples/<group>_v1alpha1_<kind>.yaml`

Refer to the [OpenAPI Specification](https://swagger.io/specification/) for further information about the possibilities of the schema.
