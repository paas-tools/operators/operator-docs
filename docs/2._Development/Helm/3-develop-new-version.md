# Develop a new version of the Helm operator

## Working on the Helm chart

First step is to develop and test the Helm chart without deploying it via the operator. E.g. on an OKD4 dev cluster
configured with flavor `paas` or `app-catalogue`:

```bash
export KUBECONFIG=/my/dev/cluster/admin/kubeconfig
# create project on behalf of CERN user to have SSO integration working
oc new-project test-nexus --description "test nexus cern chart" --as=alossent --as-group system:authenticated:oauth --as-group system:authenticated
# deploy/upgrade our Helm chart, setting some Helm values
helm upgrade --namespace test-nexus --install nexus1 helm-charts/nexus-cern --set cern-auth-proxy.route.hostname=nexus1.apptest.cern.ch --set init-nexus-config.initialAdminEgroup=it-service-vcs --set nexus-repository-manager.nexus.securityContext=null --reset-values
```

NB: when adding a new resource kind to the Helm chart, don't forget to give the operator controller-manager permissions
to manage the new resource kind! (in `config/rbac/role.yaml`)

## Working on the operator itself

Once we have a working Helm chart, we can now deploy an instance of the application using the operator.

Still on an OKD4 dev cluster configured with flavor `paas` or `app-catalogue`:

```bash
export KUBECONFIG=/my/dev/cluster/admin/kubeconfig
# deploy CRD to dev cluster
make install
# run the operator locally
make run
```

Now the operator is running (on our local machine, not on the cluster) and watches for custom resources.

NB1: the operator runs under your admin account, so it has all permissions! But when deployed with OLM it will only
have the permissions granted by `config/rbac/role.yaml`.

NB2: if another version of the operator is installed in the cluster, stop it before running the operator locally!
Otherwise they will compete with one another.

Create an application instance via the operator:

```bash
# create another project for an instance provisioned from operator
oc new-project test-nexus2 --description "test nexus cern operator" --as=alossent --as-group system:authenticated:oauth --as-group system:authenticated
# create app instance by instantiating a custom resource
# (assuming we've properly updated the sample when creating our CRD)
oc create -f config/samples/nexus_v1alpha1_nexus.yaml
```


## Packaging the operator with OLM

In order to iteratively develop the OLM packaging of the operator, we must take into account the following concepts:

* The `controller manager`.
* The `bundle`: a representation of the operator in the form of manifests. It will be composed of several resources, like the `CustomResourceDefinition` (CRD), the `ClusterServiceVersion` (CSV), and the necessary service account and RBAC for the controller manager to run.
* The `catalog index`: the catalog index will be composed of the different bundles (versions) of the operator we want to deploy in our dev/prod clusters.
* The `CatalogSource`, `Subscription` and `OperatorGroup` openshift resources for this specific operator, that will allow the operator to be deployed according to the CSV configuration.

Check <https://docs.okd.io/latest/operators/admin/olm-managing-custom-catalogs.html> for a more detailed description.

To develop a new version of the Helm operator, we will lean in the use of the GitLab CI jobs prepared for the Helm operators purpose (see <https://gitlab.cern.ch/paas-tools/infrastructure-ci/-/blob/master/operator-ci-templates/DockerImages.gitlab-ci.yml>). We will basically simulate how the GitLab CI does but in our local development machine.

These commands are based on the `Makefile` provided by default by the helm operator generated project.

!!! warning
    The `Makefile` file should never be edited, and it's is strongly recommended to leave it as is.
    In case you do need any particular modification, try to fit it under <https://gitlab.cern.ch/paas-tools/infrastructure-ci/-/blob/master/operator-ci-templates/DockerImages.gitlab-ci.yml>.

First version of the operator (`0.0.1`) is a special case for the OKD4 infrastructure, given [how the catalog sources are deployed](https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/docs/components/operators-catalogue), so we will deal with it separately.

### Testing the OLM deployment on a dev cluster (optional)

This step can be used to debug the OLM packaging process.
In general it's optional since once we are happy with our operator using the previous steps,
we can just commit the changes and let GitLab CI build the OLM catalog (see "committing changes" sections below)
and then [deploy it on app-catalogue staging or prod instances](../4._Deployment/Helm/1-deploy-new-version.md).

Let's assume we have already deployed the version `0.0.1`, and we want to deploy the version `0.0.2`.

!!! info
    Successives commands assume you have a running container as explained in [../../1._Getting_Started/Helm/1-development-environment.md]

Take the following in consideration:

* We need to define the different image tags for the `controller manager`, `bundle` and the `catalog`. For development purposes, we will create the tag `:dev` for the catalog. For staging/production purposes, we will use `:staging` and `:latest` tags for the catalog images respectively. This is to ensure that automatic updates of the operator are happening without performing any change at the cluster level (i.e., by creating a new MR with the new version of the catalog).
* We will deploy the resulting images under the [Harbor](https://registry.cern.ch) registry.

```bash
export VERSION=0.0.<X> # this example will be export VERSION=0.0.2
export IMAGE_TAG_BASE="registry.cern.ch/wordpress/<operator-name>/<operator-name>"
export IMG="${IMAGE_TAG_BASE}-controller:${VERSION}"
export CATALOG_IMG=${IMAGE_TAG_BASE}-catalog:dev
export CATALOG_BASE_IMG=${IMAGE_TAG_BASE}-catalog:dev
export BUNDLE_IMG=${IMAGE_TAG_BASE}-bundle:v${VERSION}
export BUNDLE_CHANNELS="--channels='stable'"
export BUNDLE_DEFAULT_CHANNEL="--default-channel='stable'"
export BUNDLE_METADATA_OPTS="${BUNDLE_CHANNELS} ${BUNDLE_DEFAULT_CHANNEL}"
export BUNDLE_IMGS=${BUNDLE_IMG}

# Login into registry.cern.ch
# The <robot-account-token> value is the result of generating a robot account for the specific project in the Harbor registry.
# It must be kept under the CI variables.
export DOCKER_AUTH_CONFIG="{\"auths\":{\"registry.cern.ch\":{\"auth\":\"<robot-account-token>\"}}}"
mkdir -p ~/.docker/ && echo "${DOCKER_AUTH_CONFIG}" > ~/.docker/config.json

# Generate controller image and push it to the registry.
make docker-build docker-push
# Generate, build and push the bundle manifests.
make bundle bundle-build bundle-push

# Or altogether
make docker-build docker-push bundle bundle-build bundle-push
```

At this point, we will be able to test our bundle in a dev cluster. Note that is not recommended to perform any change under the generated `bundle` folder. Everything is auto-generated from the `config/` folder:

```bash
# Connect to a development cluster
# First download oc
curl -L https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable-4.7/openshift-client-linux.tar.gz | tar xvz -C /tmp && mv /tmp/oc /usr/local/bin
# You can also `export KUBECONFIG=<path to existing kubeconfig file>`
oc login ...
# Create a namespace where your operator and all associated resources will land
# (`CatalogSource`, `Subscription`, `OperatorGroup`, etc.) in your local cluster:
oc create namespace openshift-cern-<operator-name>

# Run tests for the operator
# NB: the `olm-spec-descriptors-test` will complain about missing specDescriptors; we can ignore it:
# instead we set up our OKD clusters so the OKD console generates a form automatically
# as per https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/466
operator-sdk scorecard ${BUNDLE_IMG}

# Deploy the operator in your dev cluster.
operator-sdk run bundle ${BUNDLE_IMG} -n openshift-cern-<operator-name>
# sample output
# INFO[0008] Successfully created registry pod: h-wordpress-<operator-name>-<operator-name>-bundle-v0-0-2
# INFO[0008] Created CatalogSource: <operator-name>-catalog
# INFO[0008] OperatorGroup "operator-sdk-og" created
# INFO[0008] Created Subscription: <operator-name>-v0-0-2-sub
# INFO[0012] Approved InstallPlan install-jslw9 for the Subscription: <operator-name>-v0-0-2-sub
# INFO[0012] Waiting for ClusterServiceVersion "openshift-cern-<operator-name>/<operator-name>.v0.0.2" to reach 'Succeeded' phase
# INFO[0012]   Waiting for ClusterServiceVersion "openshift-cern-<operator-name>/<operator-name>.v0.0.2" to appear
# INFO[0023]   Found ClusterServiceVersion "openshift-cern-<operator-name>/<operator-name>.v0.0.2" phase: Pending
# INFO[0024]   Found ClusterServiceVersion "openshift-cern-<operator-name>/<operator-name>.v0.0.2" phase: Installing
# INFO[0033]   Found ClusterServiceVersion "openshift-cern-<operator-name>/<operator-name>.v0.0.2" phase: Succeeded
# INFO[0033] OLM has successfully installed "<operator-name>.v0.0.2"

# try creating a CR ...
oc new-project testproject --description "test"

# Sample case for WordPress. Adapt it the specifications of your operator.
cat config/samples/<crd-sample>.yaml | sed 's/hostname:.*/hostname: testgrafana.webtest.cern.ch/' | oc create -f - -n testproject

# For cleanup, consider the following.
oc delete project testproject
operator-sdk cleanup <operator-name> -n openshift-cern-<operator-name>
# To do a reset of the CRD plus the namespace, use this in addition.
kustomize build config/default | oc delete -f -

# In case you are manually adding CatalogSource, Subscription, etc. (not recommended)
# oc delete CatalogSource/<operator-name> -n openshift-cern-<operator-name>
# oc delete OperatorGroup/<operator-name> -n openshift-cern-<operator-name>
# oc delete Subscription/<operator-name> -n openshift-cern-<operator-name>
# oc get csv -A
# oc delete csv/<operator-name>.v0.0.<X> -n openshift-cern-<operator-name>
```

If for some reason we are experiencing issues with our helm manifests, we can consider:

```bash
# Note that in helm is not included in the Makefile. You would need to download it separately if needed.
# Check https://helm.sh/docs/intro/install/
helm template helm-charts/<name>

# To just run the controller manager in the dev cluster, execute:
make run
```

Once we are satifisfied with the result, it is time to commit changes.

Depending on the version we want to deploy, different procedure applies. See below.

### Committing changes for the first version of the operator

When committing the first version of the operator, there is no existing catalog at that moment, hence we cannot _append_ the new bundle but _add_ it to the catalog.

* Update the `VERSION` value under the `.gitlab-ci.yml` file.
  
  ```yaml
  variables:
    VERSION: 0.0.1
    ...
  ```

* Set the `CATALOG_BASE_IMG` variable to `""` under the `.gitlab-ci.yml` file (within the docker container for developing, use `unset CATALOG_BASE_IMG`).

  ```yaml
    ---
    variables:
      VERSION: 0.0.1
      ...
      # Set CATALOG_BASE_IMG to an existing catalog image tag to add $BUNDLE_IMGS to that image.
      CATALOG_BASE_IMG: '""' # for very first version, then use the one below for following versions
      # CATALOG_BASE_IMG: ${IMAGE_TAG_BASE}-catalog:latest
  ```

* Run `make bundle` to initialize the clusterserviceversion file, the first time it will ask to provide name,
  description etc. for the CSV. To be consistent with other operators, check their own CSV.

* Ensure removing the annotation `olm.skipRange` value under `config\manifests\bases\<operator-name>.clusterserviceversion.yaml`.

### Committing changes for sucessive versions of the operator

* Update the `VERSION` value under the `.gitlab-ci.yml` file.

  From:

  ```yaml
  variables:
    VERSION: 0.0.1
    ...
  ```

  To:

  ```yaml
  variables:
    VERSION: 0.0.2
    ...
  ```

* Ensure the `CATALOG_BASE_IMG` variable is set under the `.gitlab-ci.yml` file.

  ```yaml
    ---
    variables:
      VERSION: 0.0.2
      ...
      # Set CATALOG_BASE_IMG to an existing catalog image tag to add $BUNDLE_IMGS to that image.
      CATALOG_BASE_IMG: ${IMAGE_TAG_BASE}-catalog:latest
  ```

* Set the annotation `olm.skipRange` value under `config\manifests\bases\<operator-name>.clusterserviceversion.yaml`. E.g. if new `VERSION` is `0.0.2`, annotation `olm.skipRange` MUST be `<0.0.2`.
  Ref: <https://github.com/operator-framework/operator-lifecycle-manager/blob/master/doc/design/how-to-update-operators.md#skiprange>

* Run `VERSION=0.0.2 make bundle` to update the clusterserviceversion under version control. While it's not strictly necessary since CI will
  run it again, this allows having an up-to-date view of the final CSV under version control (this final CSV is in
  `bundle/manifests`)

Finally, commit the changes and let the CI do the work for us, i.e., to build and push both the bundle and the catalog, this last containing the bundle we have generated to the appropriate production clusters.

Refer to the [4._Deployment/Helm/1-deploy-new-version.md](../4._Deployment/Helm/1-deploy-new-version.md) document to deploy changes into the corresponding staging/production clusters.
