# Development

In order to develop a Helm operator, let's go through the following topics.

1. [Create a Helm Operator](1-create-helm-operator.md)
2. [Define the CRD of the operator](2-define-helm-operator.md)
3. [Develop a new version](3-develop-new-version.md)
4. [Deploy your operator](../../4._Deployment/Helm/1-deploy-new-version.md)
