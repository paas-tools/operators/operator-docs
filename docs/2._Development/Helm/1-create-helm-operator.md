# Create a Helm Operator

In order to create a new Helm operator, refer to the upstream documentation:

- <https://sdk.operatorframework.io/docs/building-operators/helm/tutorial/#create-a-new-project>

E.g. for an application template `Nexus` for the app-catalogue, use the following domain and group so that each operator has its own group:  

```bash
operator-sdk init --plugins helm --domain webservices.cern.ch --group nexus --version v1alpha1 --kind Nexus
```

You can create a Helm operator from scratch, or create one based on an existing chart.

!!! info
    Note that it is important to properly define the name of the root folder, since the name of this value will be used by the scaffolding code generator.

## Set up RBAC for the operator

The operator will need to be granted explicit permissions to manage the resources created by the Helm chart.
It's critical to update `config/rbac/role.yaml` with the necessary permissions!

For an OKD app, it will typically be necessary to add these RBAC rules to manage ingress with routes. Notice we need
`routes/custom-host` to set the route hostname explicitly!

```yaml
rules:
- apiGroups:
  - route.openshift.io
  resources:
  - routes
  - routes/custom-host
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
```

The default set of rules may not be sufficient. An easy method to determine the resources your operator needs
permissions to manage is to `helm template` your Helm chart and grep the output for all generated types of resources.

```bash
helm template helm-charts/nexus-cern | egrep -e '^apiVersion' -e '^kind:'
```

## Set up resources for the operator pod

Default resources will almost certainly be insufficient for the operator, it will run out of memory when watching resources.

Locate the TODO in `config/manager/manager.yaml` and set custom resources (adjust from observed values once the operator is running)

```yaml
        resources:
              requests:
                cpu: "100m"
                memory: "256Mi"
              limits:
                cpu: "2"
                memory: "1Gi"
```

## Set up CI

Use the following `.gitlab-ci.yml`, updating `IMAGE_TAG_BASE`.

- For deployment with OLM on app-catalogue, several images are built from the `IMAGE_TAG_BASE` prefix:
  - the controller itself 
  - a bundle image with the metadata about the current version (CSV etc.)
  - the catalog image (for deployment to staging and prod clusters) where each bundle is appended
- [set up a project](https://gitlab.cern.ch/vcs/harbor-integration-docs/-/blob/master/docs/1._Getting_Started/1-setup-harbor-project.md)
  on `registry.cern.ch`
  and [set CI/CD var `DOCKER_AUTH_CONFIG`](https://gitlab.cern.ch/vcs/harbor-integration-docs/-/blob/master/docs/2._Build_Push_And_Run_Image/2-using-gitlab-ci-cd.md)
  (CI templates for Helm operators do not expect gitlab-registry).


```yaml
variables:
  VERSION: 0.0.1
  # IMAGE_TAG_BASE defines the docker.io namespace and part of the image name for remote images.
  # This variable is used to construct full image tags for bundle and catalog images.
  IMAGE_TAG_BASE: "registry.cern.ch/vcs/nexus-cern-operator/nexus-cern-operator"
  # Image URL to use all building/pushing image targets
  IMG: "${IMAGE_TAG_BASE}-controller:${VERSION}"
  BUNDLE_IMG: ${IMAGE_TAG_BASE}-bundle:v${VERSION}
  # Channel naming
  # c.f. https://olm.operatorframework.io/docs/best-practices/channel-naming/#recommended-channel-naming
  # Bundle channels are configured per enviroment
  BUNDLE_CHANNELS: "--channels='stable'"
  BUNDLE_DEFAULT_CHANNEL: "--default-channel='stable'"
  BUNDLE_METADATA_OPTS: "${BUNDLE_CHANNELS} ${BUNDLE_DEFAULT_CHANNEL}"
  # A comma-separated list of bundle images (e.g. make catalog-build BUNDLE_IMGS=example.com/operator-bundle:v0.1.0,example.com/operator-bundle:v0.2.0).
  # These images MUST exist in a registry and be pull-able.
  BUNDLE_IMGS: ${BUNDLE_IMG}
  # Operator SDK version required.
  OPERATOR_SDK_VERSION: 1.19.0

include:
  - project: 'paas-tools/infrastructure-ci'
    file: 'operator-ci-templates/DockerImages.gitlab-ci.yml'

# DEVELOPMENT PURPOSES #
#
# Just use it for development purposes.
Build and Push Bundle (for development purposes):
  stage: build-bundle
  environment: dev
  except:
    - tags
  when: manual
  needs: []

# STAGING #
#
# When pushing to any branch, this will trigger a deployment under the `stable` channel.
# and to the `catalog:staging` tag (staging clusters)
# i.e., changes are automatically deployed to https://app-stg-cat.cern.ch
# This process must be triggered manually and it will build/push the controller, the bundle and the catalog.
OLM Deployment (staging):
  stage: deploy-bundle-staging
  environment: staging
  except:
    - tags
  when: manual
  needs: []
  variables:
    # Set CATALOG_BASE_IMG to an existing catalog image tag to add $BUNDLE_IMGS to that image.
    # Use staging tag for staging clusters
    CATALOG_IMG: ${IMAGE_TAG_BASE}-catalog:staging
    CATALOG_BASE_IMG: '""' # for very first version, then use the value below for following versions
#    CATALOG_BASE_IMG: ${IMAGE_TAG_BASE}-catalog:staging

# PRODUCTION #
#
# When generating a tag (e.g., 0.0.3), this will trigger a deployment under the `stable` channel.
# and to the `catalog:latest` tag (production clusters)
# i.e., changes are automatically deployed to https://app-catalogue.cern.ch
# This process must be triggered manually and it will build/push the controller, the bundle and the catalog.
OLM Deployment (production):
  stage: deploy-bundle-production
  environment: production
  when: manual
  needs: []
  only:
    - tags
  variables:
    # Set CATALOG_BASE_IMG to an existing catalog image tag to add $BUNDLE_IMGS to that image.
    # Use latest tag for production clusters
    CATALOG_IMG: ${IMAGE_TAG_BASE}-catalog:latest
    CATALOG_BASE_IMG: '""' # for very first version, then use the value below for following versions
#    CATALOG_BASE_IMG: ${IMAGE_TAG_BASE}-catalog:latest
```

